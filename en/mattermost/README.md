# Framateam

[Framateam](https://framateam.org) is a free chat service for group communication. It supports notifications, and lets you archive past conversations and search through them.

It is a deployment of the free software [Mattermost](http://mattermost.org). Please consult the [official documentation](https://docs.mattermost.com) of Mattermost.

To know which version of Mattermost is used by Framateam, use the main menu (menu principal) by clicking on your pseudonym in the top of the menu bar, and look in the "About Mattermost" ("À Propos") section.

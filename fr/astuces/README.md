# Astuces

Cette section souhaite répondre aux questions générales liées à l'informatique. Pour des astuces relatives aux services que nous proposons, vous trouverez parfois une section **Astuces** dans leur documentation respective.

## Créer un lien dans un mail

Pour éviter des soucis de copié/collé des liens ou les modifications de ceux-ci par les logiciels mails, vous pouvez insérer un lien dans un mot ou une phrase.

### Avec Thunderbird

Pour ce faire, dans un message, vous devez sélectionner le mot ou la phrase en double-cliquant dessus ou en maintenant le clic, puis&nbsp;:

  * cliquer sur **Insérer**

  ![image montrant comment insérer un lien dans thunderbird](images/astuces_thunderbird_inserer_lien.gif)
  * entrer le lien qui sera lié au mot (ici **site**)

  ![image montrant les propriétés du lien à insérer dans Thunderbird](images/astuces_thunderbird_inserer_lien_proprietes.png)
  * cliquer sur **OK**

La personne recevant le mail aura le lien inséré directement dans le mot (visible dans la barre inférieure en survolant le mot)&nbsp;:

![image montrant un lien survolé](images/astuces_thunderbird_lien_insere.gif)

### Avec gmail

Pour ce faire, vous devez sélectionner le mot ou la phrase en double-cliquant dessus ou en maintenant le clic, puis&nbsp;:

  * cliquer sur l'icône «&nbsp;lien&nbsp;»

  ![image montrant l'icône pour créer un lien](images/astuces_gmail_lien.gif)

  * ajouter le lien à insérer sous **À quelle URL ce lien est-il associé ?**
  * cliquer sur **OK**

  ![image montrant comment insérer le lien dans la phrase sélectionnée](images/astuces_gmail_lien.png)

## Fournir les infos de débogage de DAVx5

Pour aider à comprendre un problème nous pouvons avoir besoin des informations fournies par l'application. Pour les obtenir vous devez&nbsp;:

  1. cliquer sur le menu <i class="fa fa-bars" aria-hidden="true"></i>
  * cliquer sur <i class="fa fa-cog" aria-hidden="true"></i> **Paramètres**
  * cliquer sur <i class="fa fa-bug" aria-hidden="true"></i> **Afficher les infos de débogage**

A partir de là, vous pouvez fournir les informations en cliquant sur l'icône de partage <i class="fa fa-share-alt" aria-hidden="true"></i> et sélectionner votre application mail ou copier les données et les coller dans un [Framabin](https://framabin.org/) et nous donner l'adresse de celui-ci.

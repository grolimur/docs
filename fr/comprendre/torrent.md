# Comprendre le téléchargement en torrent

Chaque personne dispose d’un photocopieur.

Une seule personne a un livre. Lorsque d’autres personnes lui demandent une copie du livre, elle commence à le photocopier page par page et à distribuer les pages autour d’elle. Dès qu’une personne a reçu une page, elle la photocopie à son tour pour la redistribuer à d’autres qui ne l’ont pas encore.

On se rend compte très rapidement d’une chose : au début, on doit attendre pour avoir des pages pour commencer à photocopier à son tour, mais plus il y a de pages qui ont été photocopiées et distribuées, plus il y a de personnes qui peuvent à leur tour les photocopier et les distribuer.

De plus, on n’est pas obligé de photocopier les pages dans l’ordre. On annonce les pages que l’on a, quelqu’un nous demande telle ou telle page (qu’il n’a pas encore), on lui photocopie. Et on demande à son tour des pages manquantes aux autres personnes pour compléter son propre livre.

A la fin, tout le monde a son livre complet et reste disponible pour photocopier et distribuer toutes les pages.

Si quelqu’un de nouveau arrive et souhaite à son tour un exemplaire du livre, différentes personnes photocopient différentes parties pour lui donner les pages. La personne reçoit un livre complet beaucoup plus rapidement.

# Torrent

Quand on parle de téléchargement via un Torrent, c’est ce qui se passe. On lance son logiciel, l’équivalent d’une photocopie, les échanges des pages de livres se font par Internet. Au niveau du vocabulaire :
* on a les "seeders" qui sont les personnes qui ont déjà le livre complet et qui peuvent le photocopier et partager des pages.
* et les "leechers" qui sont celles qui n’ont que des bouts de livres, qui le complètent peu à peu en commençant déjà à photocopier (repartager) les parties qu’elles ont déjà.

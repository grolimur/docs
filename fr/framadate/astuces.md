# Astuces

Framadate classique permet d'insérer des images pour faire un sondage. Pour cela, il faut que les images soient déjà en ligne. Si elles sont sur votre ordinateur, vous devez les mettre sur un hébergeur d'images. Dans notre exemple, celui-ci sera [Framapic](https://framapic.org/).

**Sur Framapic** :

  * cliquez sur **Cliquez pour utiliser le navigateur de fichier**
  * sélectionnez les photos à insérer (maintenez la touche `ctrl` en cliquant sur les photos pour en sélectionner plusieurs)
  * cliquez sur **Ouvrir** pour les envoyer sur Framapic

Vous obtenez alors les liens vers vos photos. Les liens utiles pour Framadate sont ceux fléchés :

![Lien markdown framapic pour insérer dans framadate](img/framapic_photo_date.png)

En copiant ces liens et en les collant dans Framadate :

![Liens markdown des images dans Framadate](img/date_classique_liens_md.png)

Vous obtenez l'affichage des images :

![Affichage des images dans Framadate](img/date_classique_images.png)

 # Intégration des pages GitLab avec Let's Encrypt

<div class="alert alert-warning">
<img src="images/200px-Flag_of_the_United_Kingdom.svg.png" width="50px" height="25px" />
Gitlab now natively providing Let's Encrypt certificates to the custom domains of the Gitlab Pages, we have cut our service which did it automatically for you.

It is imperative, for your custom domain to continue to have an up-to-date Let's Encrypt certificate, to enable the native functionality in your project settings. This activation will replace your current certificate with a new Let's Encrypt certificate managed by Gitlab.

See <a href="https://docs.gitlab.com/ce/user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.html" title="Link to GitLab tutorial">this tutorial</a>
</div>

 <div class="alert alert-warning">
   <p>
     Framasoft se chargeait précédemment, jusqu’à la version 12.1 de Gitlab, de créer et ajouter automatiquement un certificat Let's Encrypt aux domaines personnalisés des projets qui en avaient. L’utilisation de certificat Let’s Encrypt étant désormais native, le service a été coupé.
   </p>
   <p>
     Il est <strong>impératif</strong>, pour que votre domaine personnalisé continue à avoir un certificat Let’s Encrypt à jour, d’activer la fonctionnalité native dans les paramètres de votre projet. Cette activation remplacera votre certificat Let’s Encrypt actuel par un nouveau certificat Let’s Encrypt géré par Gitlab.
   </p>
</div>

 L'intégration de GitLab Pages avec Let's Encrypt (LE) vous permet d'utiliser les certificats LE pour vos *Pages* avec des domaines personnalisés sans avoir à les émettre et les mettre à jour vous-même ; GitLab le fait pour vous, prêt à l'emploi.

 Let's Encrypt est une autorité de certification gratuite, automatisée et open source.

 <p class="alert alert-warning">
 Attention : cette fonctionnalité est en version bêta et peut présenter des bogues et des problèmes UX.
 </p>

 ## Prérequis

 Avant de pouvoir activer le déploiement automatique d'un certificat SSL pour votre domaine, assurez-vous que vous avez :

  * créé un projet Framagit contenant le code source de votre site
  * un domaine (`exemple.tld`) et [ajouté les entrées DNS](gitlab-pages.html#configuration-dns) pointant vers vos pages Framagit
  * [ajouté votre domaine aux Pages Framagit](gitlab-pages.html#ajouter-un-domaine)
  * votre site web opérationnel, accessible via votre domaine personnalisé

## Activation de l'intégration de Let's Encrypt pour votre domaine personnalisé

Une fois les prérequis satisfaits, vous devez&nbsp;:

  1. aller dans **Paramètres** > **Pages**
  * trouver votre domaine et cliquer sur **Détails**
  * cliquer sur **Edit**
  * activer l'intégration de Let's Encrypt en changeant le bouton **Automatic certificate management using Let’s Encrypt**&nbsp;:
    ![activation de LE](images/gitlab-pages-le-activation.png)
  * sauvegarder les changements

Une fois activé, GitLab obtiendra un certificat LE et l'ajoutera au domaine **Pages** associé. Il sera également renouvelé automatiquement par GitLab.

<div class="alert alert-info">
<b>Notes</b>&nbsp;:
<ul>
  <li>l'émission du certificat et la mise à jour de la configuration des pages peuvent prendre <b>jusqu'à une heure</b>.</li>
  <li>si vous avez déjà un certificat SSL dans les paramètres du domaine, il continuera à fonctionner jusqu'à ce qu'il soit remplacé par le certificat Let's Encrypt</li>
</ul>
</div>

## Problèmes

### Erreur "Le certificat ne contient pas d'intermédiaires"

Si vous rencontrez cette erreur alors que vous essayez d'activer l'intégration de Let's Encrypt pour votre domaine, suivez les étapes ci-dessous&nbsp;:

  1. allez dans **Paramètres** > **Pages**
  * décochez **Force HTTPS (requires valid certificates)** si coché
  * cliquez sur **Details** pour votre domaine
  * cliquez sur le bouton **Edit** dans le coin supérieur droit de la page de détails de votre domaine
  * activez l'intégration Let's Encrypt
  * cliquez sur **Save**
  * allez dans **Paramètres** > **Pages**
  * cochez **Force HTTPS (requires valid certificates)**

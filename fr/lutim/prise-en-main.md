# Prise en main

## Modifier un date d'expiration

Pour modifier une date d'expiration vous devez, **depuis le même navigateur que celui utilisé pour l'envoi de l'image**, aller dans l'onglet **Mes images**, puis&nbsp;:

  1. cliquer sur l'icône <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
  * changer la date
  * cliquer sur **Enregistrer les modifications**

![image montrant comment éditer une date](images/lutim_edit_date_fr.png)

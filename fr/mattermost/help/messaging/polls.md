# Sondages

<p class="alert alert-warning">À l’heure où cette documentation a été écrite (9 avril 2019), la traduction française n’était pas encore disponible. Une <a href="https://github.com/matterpoll/matterpoll/pull/145">pull request</a> a été créée par nos soins, il ne reste plus qu’à attendre son intégration et la publication d’une mise à jour la contenant.</p>

Vous pouvez créer des sondages dans Framateam. Pour cela vous devez utiliser la commande `/poll`&nbsp;:&nbsp;`/poll "question" "réponse 1" "réponse 2" "réponse3"`.

Par exemple `/poll "On fait un sondage ?" "Oui" "Non" Pantoufle"` donnera&nbsp;:

![image présentation d'un sondage](../../images/team_sondage.png)

Pour stopper les votes vous devez cliquer sur **Terminer le sondage**. Vous devez alors cliquer sur le lien présenté pour voir les résultats&nbsp;:

> Le sondage **On fait un sondage ?** est clos et le message originel a été mis à jour. Vous pouvez y accéder en cliquant **ici**.

![image montrant les résultats d'un vote](../../images/team_sondage_resultats.png)

## Sondage rapide

Il est aussi possible de faire un sondage rapide ne nécessitant qu'un **Oui** ou un **Non**. Pour cela il ne faut mettre que la question&nbsp;:

`/poll "Danse ?"`

![image d'un sondage rapide](../../images/team_sondage_rapide.png)

## Options

  * `--anonymous` : ne montre pas qui a voté pour quoi à la fin (`/poll "On fait un sondage ?" "Oui" "Non" Pantoufle" --anonymous`)
  * `--progress` : montre le nombre de votes par réponse durant le sondage (`/poll "On fait un sondage ?" "Oui" "Non" Pantoufle" --progress`)

Il est bien sûr possible de cumuler les options&nbsp;: `/poll "On fait un sondage ?" "Oui" "Non" Pantoufle" --anonymous --progress`
